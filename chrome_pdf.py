import sys, subprocess

class ChromeHTMLToPDF():
    # set headless chrome arguments
    _chrome_args = (
        "{chrome_exec}",
        "--headless",
        "--crash-dumps-dir='{temp_dir}/'"
        "--disable-gpu",
        "--no-margins",
        "--print-to-pdf='{output_file}'",
        "'{input_file}'"
    )

    def __init__(self, chrome_exec, temp_dir):
        '''
        Constructor
        chrome_exec (string) - path to chrome executable
        '''

        # check if the chrome executable path is non-empty string
        assert isinstance(chrome_exec,str) and chrome_exec != ''

        # check if temp dir is a string
        assert isinstance(temp_dir, str)

        self._chrome_exec = chrome_exec
        self._temp_dir = temp_dir


    def print_html_to_pdf(self, input_filename, output_filename):
        '''
        Converts the given html to PDF and stores as output_file
        input_filename (string) - location to html file to be rendered
        output_filename (string) - file name for output PDF file
        raise Error if not successful
        '''

        # checks if the input_filename string given is a valid string
        assert isinstance(input_filename, str)

        # prepare the shell command
        print_to_pdf_command = ' '.join(self._chrome_args).format(
            chrome_exec=self._chrome_exec,
            temp_dir=self._temp_dir,
            input_file=input_filename,
            output_file=output_filename
        )

        try:
            # execute the shell command to generate PDF
            subprocess.run(print_to_pdf_command, shell=True, check=True)
        except subprocess.CalledProcessError as e:
            raise e
