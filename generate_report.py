import sys
import base64
import datetime
import os

from pdf_manager import URLtoPDFManager
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import (
    Mail, Attachment, FileContent, FileName,
    FileType, Disposition, ContentId)

# comments:
# - gd has no HTTP data
# - dsicloud2 has no HTTP data
# - Missing Hive and PostgreSQL Latency data from nagios

urls = [
    # ADMS
    {
        "url": "http://sng.usc.edu/nagios/cgi-bin/avail.cgi?host=adms&assumeinitialstates=yes&assumestateretention=yes&assumestatesduringnotrunning=yes&includesoftstates=no&initialassumedhoststate=0&initialassumedservicestate=0&timeperiod=last31days&backtrack=4",
        "requires_authentication": True
    },
    {
        "url": "http://sng.usc.edu/nagiosgraph/cgi-bin/show.cgi?host=adms&service=PING&period=month&geom=1000x200",
        "requires_authentication": False
    },
    {
        "url": "http://sng.usc.edu/nagiosgraph/cgi-bin/show.cgi?host=adms&service=HTTP&period=month&geom=1000x200",
        "requires_authentication": False
    },
    {
        "url": "http://sng.usc.edu/nagiosgraph/cgi-bin/show.cgi?host=adms&service=SSH&period=month&geom=1000x200",
        "requires_authentication": False
    },
    # GD
    {
        "url": "http://sng.usc.edu/nagios/cgi-bin/avail.cgi?host=gd&assumeinitialstates=yes&assumestateretention=yes&assumestatesduringnotrunning=yes&includesoftstates=no&initialassumedhoststate=0&initialassumedservicestate=0&timeperiod=last31days&backtrack=4",
        "requires_authentication": True
    },
    {
        "url": "http://sng.usc.edu/nagiosgraph/cgi-bin/show.cgi?host=gd&service=PING&period=month&geom=1000x200",
        "requires_authentication": False
    },
    {
        "url": "http://sng.usc.edu/nagiosgraph/cgi-bin/show.cgi?host=gd&service=SSH&period=month&geom=1000x200",
        "requires_authentication": False
    },
    {
        "url": "http://sng.usc.edu/nagiosgraph/cgi-bin/show.cgi?host=gd&service=Disk%201%20Usage&period=month&geom=1000x200",
        "requires_authentication": False
    },
    {
        "url": "http://sng.usc.edu/nagiosgraph/cgi-bin/show.cgi?host=gd&service=Disk%202%20Usage&period=month&geom=1000x200",
        "requires_authentication": False
    },
    # DSICLOUD
    {
        "url": "http://sng.usc.edu/nagios/cgi-bin/avail.cgi?host=dsicloud2&assumeinitialstates=yes&assumestateretention=yes&assumestatesduringnotrunning=yes&includesoftstates=no&initialassumedhoststate=0&initialassumedservicestate=0&timeperiod=last31days&backtrack=4",
        "requires_authentication": True
    },
    {
        "url": "http://sng.usc.edu/nagiosgraph/cgi-bin/show.cgi?host=dsicloud2&service=PING&period=month&geom=1000x200",
        "requires_authentication": False
    },
    {
        "url": "http://sng.usc.edu/nagiosgraph/cgi-bin/show.cgi?host=dsicloud2&service=SSH&period=month&geom=1000x200",
        "requires_authentication": False
    },
    # DB
    {
        "url": "http://sng.usc.edu/nagiosgraph/cgi-bin/showservice.cgi?service=Kafka%20Test&period=month&expand_period=month&geom=1000x200",
        "requires_authentication": False
    },
    {
        "url": "http://sng.usc.edu/nagiosgraph/cgi-bin/show.cgi?host=gd&service=PostgreSQL%20Connection&db=time%2Cdata&period=month&expand_period=month&geom=1000x200",
        "requires_authentication": False
    }
]

def main(argv):
    if len(argv) != 4:
        print("Invalid usage.")
        print("generate_report.py <username> <password> <report_name> <chrome_exec>")
        sys.exit(2)

    kwargs = {
        "username": argv[0],
        "password": argv[1],
        "chrome_exec": argv[3],
        "temp_path": "./temp",
        "base_url": "http://sng.usc.edu"
    }

    # "/Applications/Google\ Chrome.app/Contents/MacOS/Google\ Chrome"

    try:
        url_to_pdf_manager = URLtoPDFManager(**kwargs)
        url_to_pdf_manager.make_pdf(urls, argv[2])
        print("PDF successfully generated.")
        # send the report as email
        #send_mail(argv[2])
    except Exception as e:
        print(e)
        print("An error occured.")


def send_mail(report_filename):
    with open('data_report_template.html', 'r') as f:
        content = f.read()
    content_plain = '''
    Dear ADMS friends,

    Please find attached the latest data quality report.

    Best,
    ADMS Support Team
    '''

    today = datetime.date.today()

    # Prepare email.
    message = Mail(
        from_email=('adms.imsc@gmail.com', 'ADMS Support Team'),
        to_emails='adms-weekly-updates@googlegroups.com',
        subject='ADMS Weekly Data Quality Report (' + str(today) + ')',
        plain_text_content=content_plain,
        html_content=content)

    # Add attachment to email.
    with open(report_filename, 'rb') as f:
        data = f.read()
        f.close()
    encoded = base64.b64encode(data).decode()
    attachment = Attachment()
    attachment.file_content = FileContent(encoded)
    attachment.file_type = FileType('application/pdf')
    attachment.file_name = FileName('ADMS Weekly Data Quality Report.pdf')
    attachment.disposition = Disposition('attachment')
    attachment.content_id = ContentId('Report')
    message.add_attachment(attachment)

    # Send email
    sg = SendGridAPIClient(os.environ.get('SENDGRID_API_KEY'))
    response = sg.send(message)
    return response

if __name__ == "__main__":
   main(sys.argv[1:])
