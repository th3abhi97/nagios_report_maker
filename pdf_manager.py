from pywebcopy import WebPage, config
import pywebcopy

from chrome_pdf import ChromeHTMLToPDF

from PyPDF2 import PdfFileReader, PdfFileWriter

class URLtoPDFManager():

    def __init__(self, **kwargs):
        self.temp_path = kwargs.get("temp_path", "./temp")
        # use the chrome executable to print pdf
        self.chrome_pdf = ChromeHTMLToPDF(kwargs.get("chrome_exec", None), self.temp_path)
        # Set auth at at global level
        pywebcopy.SESSION.auth = (kwargs.get("username"), kwargs.get("password"))
        # initialize config for HTML fetch at global level
        config.setup_config(
            **{
                "project_url": kwargs.get("base_url", "http://sng.usc.edu"),
                "project_folder": self.temp_path,
                "project_name": "sng.usc.edu",
                "log_file": self.temp_path + "/logs.txt",
                "zip_project_folder": False,
                "over_write": True,
                "load_javascript": False,
                "bypass_robots": True
            }
        )

    # provide a list of urls to convert to pdf, and output file path
    def make_pdf(self, urls, output_file):
        assert isinstance(urls, list)

        try:
            # initialize empty output pdf
            pdf_writer = PdfFileWriter()

            # loop over all urls
            for url_obj in urls:
                # temp pdf location
                temp_pdf = self.temp_path + "/temp.pdf"
                # download url
                url = url_obj.get("url")

                # download webpage and construct pdf if auth required,
                # otherwise construct pdf directly
                html_path = url
                if url_obj.get("requires_authentication", False) is True:
                    # download webpage for each url
                    html_path = self.fetch_html_from_url(url)
                # print url to temp pdf
                self.chrome_pdf.print_html_to_pdf(html_path, temp_pdf)

                # append the temp pdf to the main pdf
                pdf_reader = PdfFileReader(temp_pdf)
                for page in range(pdf_reader.getNumPages()):
                    # add each page to the writer object
                    pdf_writer.addPage(pdf_reader.getPage(page))
                print("Downloaded url pdf :: " + url)

            # write out the merged PDF
            with open(output_file, 'wb') as out:
                pdf_writer.write(out)

        except Exception as e:
            raise e

    # fetch html and related assets into local disk
    def fetch_html_from_url(self, url):
        assert isinstance(url, str)

        try:
            wp = WebPage()
            wp.url = url
            wp.path = self.temp_path
            wp.get(url)
            wp.save_complete()
            return wp.file_path
        except Exception as e:
            raise e
